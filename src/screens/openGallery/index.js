import React, { Component } from 'react'
import { View, Text, Linking, ScrollView, Image, TouchableOpacity, Alert, CameraRoll, ImageStore } from 'react-native'
import { Spinner } from 'native-base'
import styles from './style'
import IconFA from 'react-native-vector-icons/FontAwesome'
import { staticImages } from 'helpers/common'
import ApiService from 'helpers/services'
import { SV_GET_PROFILE_DETAILS } from 'helpers/services/endpoints'
import { ThemeColor } from 'themes/variables'
import { scale, verticalScale } from 'react-native-size-matters'

class OpenGallery extends Component {
  static navigationOptions = {
    title: 'All Photos',
    headerStyle: {
      backgroundColor: ThemeColor.base,
    },
    headerTintColor: ThemeColor.textDark,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }

  constructor(props) {
    super(props)

    this.state = {
      photos: [],
      lastCursor: null,
    }
  }

  componentDidMount() {
    this.getPhotos()
  }

  convertToBase64 = (imageURI) => {
    ImageStore.getBase64ForTag(
      imageURI,
      (base64Data) => console.log(base64Data),
      (error) => console.log(error)
    )
  }

  getPhotos = () => {
    let fetchParams = {
      first: 60,
      assetType: 'Photos'
    }
    if (this.state.lastCursor) {
      fetchParams.after = this.state.lastCursor;
      console.log('here fuck: ', this.state.lastCursor)
    }
    CameraRoll.getPhotos(fetchParams).then(result => {
      console.log('lastparam: ', result.page_info.end_cursor)
      console.log('result: ', result)
      this.setState(prevState => {
        return {
          lastCursor: result.page_info.end_cursor,
          photos: [
            ...prevState.photos,
            ...result.edges
          ]
        }
      })
    })
  }

  pickImage = (image) => {
    this.convertToBase64(image)
    this.props.navigation.navigate('Profile')
  }

  render() {
    const RenderImages = () => {
      return (
        this.state.photos.map((p, index) =>
          <TouchableOpacity key={index} style={{ borderColor: 'white', borderLeftWidth: scale(1), borderBottomWidth: scale(1) }} onPress={() => this.pickImage(p.node.image.uri)}>
            <Image key={index} style={{ width: scale(86), height: scale(86) }} source={{ uri: p.node.image.uri }} />
          </TouchableOpacity>
        )
      )
    }

    const DisplayImages = ()  => {
      return (
        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
          <RenderImages />
        </View>
      )
    }

    const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
      const paddingToBottom = 20;
      return layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom
    }

    return (
      this.state.photos ?
      <ScrollView
        style={styles.mainContainer} 
        onScroll={({ nativeEvent }) => {
          if (isCloseToBottom(nativeEvent)) {
            this.getPhotos()
          }
        }}
      >
        <DisplayImages />
      </ScrollView> :
      <Spinner color={ThemeColor.main} />
    )
  }
}

export default OpenGallery