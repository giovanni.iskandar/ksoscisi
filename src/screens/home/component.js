import React, { Component } from 'react'
import { View, Image, Text, TouchableHighlight, BackHandler, Platform } from 'react-native'
import { Spinner } from 'native-base'
import { Container } from 'native-base'
import styles from './style'
import { staticImages } from 'helpers/common'
import ApiService from 'helpers/services';
import { SV_GET_PROFILE_DETAILS } from 'helpers/services/endpoints'
import Toast, { DURATION } from 'react-native-easy-toast'
import { getRouting, isIphoneX } from 'helpers/common'

const BACK_WAITING_DURATION = 1000
const BACK_COUNT_TO_EXIT = 2

class HomeScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      profileDetails: null,
      backClickCount: 0
    }
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleHardwareBack)
    }
    this.getProfileDetails()
  }

  handleHardwareBack = () => {
    let isFocused = this.props.navigation.isFocused()
    
    if(isFocused) {
      this.setState(prevState => {
        return {
          backClickCount: prevState.backClickCount + 1
        }
      })
      this.refs.toast.show('Press back again to leave', BACK_WAITING_DURATION, () => {
        // executed on toast closed
        this.setState({ backClickCount: 0 })
      })
      return true
    }
    else {
      return false
    }
  }

  componentDidUpdate(prevState) {
    if(prevState.backClickCount != this.state.backClickCount) {
      if(this.state.backClickCount == BACK_COUNT_TO_EXIT) {
        BackHandler.exitApp()
      }
    }
  }

  getProfileDetails = async () => {
    const request = {
      path: SV_GET_PROFILE_DETAILS,
      params: null
    }

    try {
      const response = await ApiService.execute(request)
      this.setState({ profileDetails: response.details })
    }
    catch(ex) {}
  }

  handleMenuClick = (menu) => {
    // BackHandler.removeEventListener('hardwareBackPress',this.handleHardwareBack)
    this.props.navigation.navigate(getRouting(menu))
  }
  
  render() {
    const { profileDetails } = this.state

    const ButtonMenu = ({ title, icon, redirect }) => {
      return (
        <TouchableHighlight
          activeOpacity={0.1}
          onHideUnderlay={() => {}}
          onShowUnderlay={() => {}}
          underlayColor="rgb(252, 232, 120)"
          style={Platform.OS === 'android' ? styles.buttonMenuContainer : styles.buttonMenuContainerIos}
          onPress={() => this.handleMenuClick(redirect)}>
          <>
            <Image source={icon} style={styles.buttonMenuIcon}/>
            <Text style={styles.buttonMenuText}>{title}</Text>
          </>
        </TouchableHighlight>
      )
    }

    const ProfileImage = () => {
      const base64Img = profileDetails.profilePicture
      return (
        <View style={styles.profileImgContainer}>
          <Image source={{uri: base64Img}} style={styles.profileImg} />
        </View>
      )
    }

    const ProfileDetails = () => {
      return (
        <View style={styles.profileDetailContainer}>
          <Text style={styles.profileNameText}>{profileDetails.name}</Text>
          <Text style={styles.profileCommodityText}>{profileDetails.commodity}</Text>
        </View>
      )
    }

    const ProfileSection = () => {
      return (
        <View style={Platform.OS === 'android' ? styles.profileContainer : styles.profileContainerIos}>
          {
            this.state.profileDetails ?
            <>
              <ProfileImage />
              <ProfileDetails />
            </>:
            <Spinner color="white" />
          }
        </View>
      )
    }

    const MenuSection = () => {
      return (
        <View style={styles.menuContainer}>
          <View style={styles.menuContainerRow}>
            <ButtonMenu title="TRACKING VO" icon={staticImages.iconMenuTrackingVo} redirect="TrackingVo" />
            <ButtonMenu title="BRANCH" icon={staticImages.iconMenuBranch} redirect="Branch" />
          </View>
          <View style={styles.menuContainerRow}>
            <ButtonMenu title="CONTACT" icon={staticImages.iconMenuContact} redirect="Contact" />
            <ButtonMenu title="AFFILIATE" icon={staticImages.iconMenuAffiliate} redirect="Affiliate" />
          </View>
        </View>
      )
    }

    return (
      <View style={styles.mainContainer}>
        <ProfileSection />
        <MenuSection />
        <Toast
          ref="toast"
          style={{ backgroundColor: 'rgba(0, 0, 0, 0.6)', borderRadius: 100, paddingHorizontal: 20 }}
          fadeInDuration={500}
          fadeOutDuration={500}
        />
      </View>
    )
  }
}

export default HomeScreen;