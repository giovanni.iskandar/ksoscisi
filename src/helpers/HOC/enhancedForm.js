import React, { Component } from 'react'
import { store } from 'helpers/redux/store'
import { setFormState } from 'helpers/redux/actions/formActions'

const enhancedForm = (WrappedComponent) => {
  return class extends Component {
    constructor(props){
      super(props)
      this.state = {
        value: this.props.value || ''
      }
    }

    updateStore = () => {
      let newState = {[this.props.name] : this.state.value};
      store.dispatch(setFormState(newState))
    }

		handleInputChange = (value) => {
			this.setState({ value }, () => {
        this.updateStore()
      })
    }
    
    componentDidUpdate(prevProps, prevState) {
      if(this.props.value != prevProps.value && !this.props.value){
        this.setState({ value: this.props.value })
      }
    }

    render() {
      
      return (
        <WrappedComponent
					{ ...this.props }
          wrapperState={this.state}
          handleInputChange={this.handleInputChange}
        />
      )
    }
  }
}

export default enhancedForm