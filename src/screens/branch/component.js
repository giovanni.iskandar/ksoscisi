import React, { Component } from 'react'
import { ScrollView, View, Text, Image } from 'react-native'
import { Spinner } from 'native-base'
import styles from './style'
import { ThemeColor } from 'themes/variables'
import ApiService from 'helpers/services'
import { SV_GET_BRANCH_LIST } from 'helpers/services/endpoints'
import BoxTitleDetail from 'components/BoxTitleDetail'

class BranchScreen extends Component {
  static navigationOptions = {
    title: 'Kantor Cabang',
    headerStyle: {
      backgroundColor: ThemeColor.main,
    },
    headerTintColor: ThemeColor.base,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }

  constructor(props) {
    super(props)

    this.state = {
      branchList: null
    }
  }

  componentDidMount() {
    this.getBranchList()
  }
  
  getBranchList = async () => {
    let request = {
      path: SV_GET_BRANCH_LIST,
      params: {}
    }

    try {
      let response = await ApiService.execute(request)
      this.setState({ branchList: response.details })
    }
    catch(ex) { }
  }

  render() {
    const { branchList } = this.state
    
    const BranchList = ({ list }) => {
      const totalItem = list.length

      return list.map((item, index) => <BoxTitleDetail key={index} data={item} index={index} totalItem={totalItem} />)
    }

    return (
      <ScrollView style={styles.mainContainer}>
        {
          branchList
          ? <BranchList list={branchList} />
          : <Spinner color={ThemeColor.main} />
        }
      </ScrollView>
    )
  }
}

export default BranchScreen;