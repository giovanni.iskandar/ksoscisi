import React, { Component } from 'react'
import { ScrollView, View, StatusBar, Text, TouchableOpacity } from 'react-native'
import { Container, Content, Header, Button, Left, Right, Body, Icon, Item, Input } from 'native-base'
import styles from './style'
import { ThemeColor } from 'themes/variables'
import { scale } from 'react-native-size-matters'
import ApiService from 'helpers/services'
import { SV_GET_VO_BY_NAME } from 'helpers/services/endpoints'
import BoxButton from 'components/BoxButton'
import { Spinner } from 'native-base'

const MIN_SEARCH_KEY = 3

class TrackingVOScreen extends Component {
  static navigationOptions = {
    title: 'VO List',
    headerStyle: {
      backgroundColor: ThemeColor.main,
      elevation: 0
    },
    headerTintColor: ThemeColor.base,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }

  state = {
    voList: null,
    isLoading: false
  }

  getVoByName = async (searchKey) => {
    let request = {
      path: SV_GET_VO_BY_NAME,
      params: {
        key: searchKey
      }
    }

    try {
      this.setState({ isLoading: true })
      let response = await ApiService.execute(request)
      this.setState({ voList: response.details, isLoading: false })
    }
    catch(ex) {
      this.setState({ isLoading: false })
    }
  }

  searchVo = (searchKey) => {
    if(searchKey.length >= MIN_SEARCH_KEY) {
      this.getVoByName(searchKey)
    }
  }

  render() {
    const { voList, isLoading } = this.state

    const VOList = ({ list }) => {
      const totalItem = list.length
      return list.map((item, index) =>
        <BoxButton 
          key={index}
          data={item}
          index={index}
          totalItem={totalItem}
          onPress={() => this.props.navigation.navigate('VoPartial', { ...item })}
        />)
    }

    return (
      <Container>
        <Header searchBar rounded style={{ backgroundColor: ThemeColor.main }}>
          <StatusBar backgroundColor={ThemeColor.main} barStyle="light-content" />
          <Item>
            <Input placeholder="Search" style={{ marginLeft: scale(10) }} onChangeText={this.searchVo} />
            <Icon name="ios-search" />
          </Item>
        </Header>
        <Content style={styles.mainContainer}>
          <ScrollView>
            {
              voList
              ? <VOList list={voList} />
              : isLoading
                ? <Spinner color={ThemeColor.main} />
                : null
            }
          </ScrollView>
        </Content>
      </Container>
    )
  }
}

export default TrackingVOScreen