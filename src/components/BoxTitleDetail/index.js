import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { Spinner } from 'native-base'
import styles from './style'
import { staticImages } from 'helpers/common'

const RowDetail = ({ icon, label }) => {
  return (
    <View style={styles.detailContainer}>
      <Image source={icon} style={styles.iconSize} />
      <View style={styles.detailLabelContainer}>
        <Text style={styles.detailLabelText}>{label}</Text>
      </View>
    </View>
  )
}

const BoxTitleDetail = ({ data, index, totalItem }) => {
  const containerStyle = index + 1 == totalItem ? styles.boxContainerLastChild : styles.boxContainer
  const base64Img = data.icon

  const titleWithImage = (
    <>
      <Image source={{uri: base64Img}} style={styles.imageFlag} />
      <View style={styles.titleTextContainer}>
        <Text style={styles.titleText}>{data.name}</Text>
      </View>
    </>
  )

  const titleOnly = <Text style={styles.titleText}>{data.name}</Text>

  return (
    <View style={containerStyle}>
      <View style={styles.titleContainer}>
        { base64Img ? titleWithImage : titleOnly }
      </View>
      <RowDetail icon={staticImages.iconGenericAddress} label={data.address} />
      <RowDetail icon={staticImages.iconGenericPhone} label={data.phone} />
      <RowDetail icon={staticImages.iconGenericFax} label={data.fax} />
      <RowDetail icon={staticImages.iconGenericEmail} label={data.email} />
    </View>
  )
}

export default BoxTitleDetail