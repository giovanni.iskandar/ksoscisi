import { connect } from 'react-redux';
import Component from './component';

const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => ({
  
});

const Container = connect(mapStateToProps, mapDispatchToProps)(Component);

export default Container;
