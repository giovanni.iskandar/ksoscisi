import { createStore, applyMiddleware } from 'redux'
import createReducer from './reducers'
import thunk from 'redux-thunk'

const logger = store => next => action => {
  console.group(action.type)
  console.info('dispatching', action)
  let result = next(action)
  console.log('next state', store.getState())
  console.groupEnd()
  return result
}

const configureStore = () => {
  const store = createStore(
    createReducer(),
    applyMiddleware(thunk, logger)
  )

  return store
}

const store = configureStore()
export { store }