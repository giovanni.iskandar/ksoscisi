import { scale, verticalScale } from 'react-native-size-matters'
import { ThemeColor, ThemeFontSize, ThemeBorder, ThemeGap } from 'themes/variables'

export default {
  boxContainer: {
    flexDirection: 'row',
    backgroundColor: ThemeColor.base,
    borderRadius: ThemeBorder.radius,
    elevation: ThemeBorder.elevation,
    marginTop: verticalScale(10),
    marginHorizontal: ThemeGap.outerHorizontal,
    paddingHorizontal: ThemeGap.innerHorizontal,
    paddingVertical: verticalScale(15),
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  get boxContainerLastChild () {
    return {
      ...this.boxContainer,
      marginBottom: verticalScale(25)
    }
  },
  rightContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  titleText: {
    fontSize: ThemeFontSize.large
  },
  badge: {
    justifyContent: 'center',
    height: scale(20),
    width: scale(20),
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: scale(10),
    alignSelf: 'center'
  },
  badgeText: {
    color: ThemeColor.base,
    fontWeight: 'bold'
  }
}