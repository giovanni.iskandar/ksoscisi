import { scale, verticalScale } from 'react-native-size-matters'
import { ThemeColor } from 'themes/variables'

export default {
  mainContainer: {
    flex: 1,
    backgroundColor: ThemeColor.original
  },
}