import React from "react";
import { View, Text } from "react-native";
import { CheckBox } from "native-base";


const CheckBoxContainer = ({ text = '', checked = false, style, onPress = () => {} }) => {
  return (
    <View style={{ flexDirection: 'row', ...style }}>
      <CheckBox checked={checked} onPress={onPress} />
      <Text style={{ marginLeft: 5 }}>{text}</Text>
    </View>
  );
}

export default CheckBoxContainer