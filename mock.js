const jsonServer  = require('json-server');
const server      = jsonServer.create();
const router      = jsonServer.router();
const middlewares = jsonServer.defaults();
router.render = (req, res) => {
  const method = req.method.toLowerCase();
	const currentPage = req.body.currentPage?req.body.currentPage:req.body.scrollPage;
  const path = req.body && currentPage ? req.url + '/' + currentPage : req.url;
  const mock = require('./mock-server' + path + '/index.' + method + '.json');
  res.status(200).jsonp(mock);
};
server.use(middlewares);
server.use(router);
server.listen(3000, () => {
  console.log('JSON Server is running')
})