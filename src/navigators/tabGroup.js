import React from 'react'
import { Text, View, FlatList } from 'react-native'
import { Content, Container } from 'native-base'
import { createBottomTabNavigator } from 'react-navigation'
import Ionicons from 'react-native-vector-icons/Ionicons'
import IconFA from 'react-native-vector-icons/FontAwesome'

import MainStack from './main'
import Profile from 'screens/profile/container'

var Icon = require('react-native-vector-icons/FontAwesome')
var myButton = (
  <Icon.Button name="facebook" backgroundColor="#3b5998" onPress={this.loginWithFacebook}>
    Login with Facebook
  </Icon.Button>
);

class AccountScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
        <Text>Your Favorite Shit!</Text>
        {myButton}
        <Ionicons name='ios-home' size={25} color='red' />
      </View>
    );
  }
}

export default createBottomTabNavigator({
  Home: {
    screen: MainStack,
    navigationOptions: ({ navigation }) => ({
      title: 'Home',
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        return <IconFA name='home' size={25} color={tintColor} />
      }
    })
  },
  Profile: {
    screen: Profile,
    navigationOptions: ({ navigation }) => ({
      title: 'Profile',
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        return <IconFA name='user-circle' size={20} color={tintColor} />
      }
    })
  }
}, {
  tabBarOptions: {
    activeTintColor: 'rgb(58, 143, 215)',
    inactiveTintColor: 'gray',
    style: {
      borderTopWidth: 0.1,
      elevation: 10
    }
  },
});