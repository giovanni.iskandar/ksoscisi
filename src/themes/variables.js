import { scale } from 'react-native-size-matters'

const ThemeColor = {
  original: 'rgb(232, 233, 237)',
  base: 'rgb(255, 255, 255)',
  main: 'rgb(58, 143, 215)',
  textLight: 'rgb(255, 255, 255)',
  textDark: 'rgb(0, 0, 0)',
  lightDark: 'rgb(210, 210, 210)'
}

const ThemeFontSize = {
  xxsmall: scale(6),
  xsmall: scale(8),
  xmall: scale(12),
  medium: scale(14),
  large: scale(16),
  xlarge: scale(20),
  xxlarge: scale(22),
  xxxlarge: scale(24)
}

const ThemeBorder = {
  radius: 12,
  elevation: 2
}

const ThemeGap = {
  outerHorizontal: scale(15),
  innerHorizontal: scale(17),
  outerVertical: scale(10),
  innerVertical: scale(12)
}

export {
  ThemeColor,
  ThemeFontSize,
  ThemeBorder,
  ThemeGap
}