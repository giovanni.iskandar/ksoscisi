import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity, StatusBar, TextInput } from 'react-native'
import { Container, Header, Title, Content, Button, Left, Right, Body, Item, Icon, Input } from 'native-base'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
import { ThemeColor } from 'themes/variables'

class AppContainer extends React.Component {
  render() {
    const defaultHeader = (
      <StatusBar
        backgroundColor={ThemeColor.main}
        barStyle="light-content" />
    )

    let selectedHeader
    switch(this.props.header){
      case 'logo':
      case 'searchBar':
      default:
        selectedHeader = defaultHeader
    }

    return (
      <Container>
        { selectedHeader }
        { this.props.children }
      </Container>
    )
  }
}
export default AppContainer