import { connect } from 'react-redux';
import Component from './component';

const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => ({
  
});

const HomeContainer = connect(mapStateToProps, mapDispatchToProps)(Component);

export default HomeContainer;
