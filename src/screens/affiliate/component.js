import React, { Component } from 'react'
import { ScrollView, View, Text, Image } from 'react-native'
import { Spinner } from 'native-base'
import styles from './style'
import { ThemeColor } from 'themes/variables'
import ApiService from 'helpers/services'
import { SV_GET_AFFILIATE_LIST } from 'helpers/services/endpoints'
import BoxTitleDetail from 'components/BoxTitleDetail'

class AffiliateScreen extends Component {
  static navigationOptions = {
    title: 'Afiliasi',
    headerStyle: {
      backgroundColor: ThemeColor.main,
    },
    headerTintColor: ThemeColor.base,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }

  constructor(props) {
    super(props)

    this.state = {
      affiliateList: null
    }
  }

  componentDidMount() {
    this.getAffiliateList()
  }
  
  getAffiliateList = async () => {
    let request = {
      path: SV_GET_AFFILIATE_LIST,
      params: {}
    }

    try {
      let response = await ApiService.execute(request)
      this.setState({ affiliateList: response.details })
    }
    catch(ex) { }
  }

  render() {
    const { affiliateList } = this.state

    const AffiliateList = ({ list }) => {
      const totalItem = list.length

      return list.map((item, index) => <BoxTitleDetail key={index} data={item} index={index} totalItem={totalItem} />)
    }

    return (
      <ScrollView style={styles.mainContainer}>
        {
          affiliateList
          ? <AffiliateList list={affiliateList} />
          : <Spinner color={ThemeColor.main} />
        }
      </ScrollView>
    )
  }
}

export default AffiliateScreen;