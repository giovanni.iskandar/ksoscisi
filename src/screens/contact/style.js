import { scale, verticalScale } from 'react-native-size-matters'
import { ThemeColor, ThemeFontSize, ThemeBorder, ThemeGap } from 'themes/variables'

export default {
  mainContainer: {
    flex: 1,
    backgroundColor: ThemeColor.original
  },
  contactContainer: {
    backgroundColor: ThemeColor.base,
    borderRadius: ThemeBorder.radius,
    elevation: ThemeBorder.elevation,
    marginTop: verticalScale(15),
    marginHorizontal: ThemeGap.outerHorizontal,
  },
  get contactContainerLastChild() {
    return {
      ...this.contactContainer,
      marginBottom: verticalScale(40)
    }
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: ThemeGap.innerHorizontal,
    paddingTop: scale(10),
    paddingBottom: scale(6),
    borderBottomWidth: 1,
    borderColor: ThemeColor.lightDark
  },
  titleText: {
    marginLeft: scale(15),
    color: ThemeColor.textDark,
    fontSize: scale(14),
    fontWeight: 'bold'
  },
  officeDetailContainer: {
    paddingHorizontal: ThemeGap.innerHorizontal,
    paddingBottom: scale(15)
  },
  officeDetailRow: {
    flexDirection: 'row',
    paddingTop: verticalScale(6),
    
  },
  officeDetailLeft: {
    flex: 2
  },
  officeDetailRight: {
    flex: 3
  },
  contactNoContainer: {
    marginVertical: verticalScale(10)
  },
  contactNoRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: scale(5),
    paddingHorizontal: ThemeGap.innerHorizontal
  },
  contactNoText: {
    color: ThemeColor.textDark,
    fontSize: ThemeFontSize.medium,
  },
  titleIcon: {
    width: scale(25),
    height: scale(25)
  }
}

