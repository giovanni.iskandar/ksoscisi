import React, { Component } from 'react'
import { View, Image, TouchableOpacity, Text, KeyboardAvoidingView } from 'react-native'
import InputText from 'components/InputFields/InputText'
import { staticImages } from 'helpers/common'
import styles from './style'
import AppContainer from 'screens/appContainer'
import ApiService from 'helpers/services';
import { SV_EXECUTE_LOGIN } from 'helpers/services/endpoints'

class LoginScreen extends Component {
  // onClickLogin = () => {
  //   const req = {
  //     path: SV_EXECUTE_LOGIN,
  //     params: {}
  //   }
  //   ApiService.execute(req).then(response => {
  //     this.props.resetFormState()
  //     this.props.navigation.navigate('Home')
  //   }).catch(error => {
  //     console.log(error)
  //   })
  // }

  // onClickLogin = () => {
  //   const req = {
  //     path: SV_EXECUTE_LOGIN,
  //     params: {}
  //   }
  //   const { dispatch, executeLogin } = this.props

  //   dispatch(executeLogin(req)).then(response => {
  //     this.props.resetFormState()
  //     this.props.navigation.navigate('Home')
  //   }).catch(error => {
  //     console.log(error)
  //   })
  // }

  onClickLogin = () => {
    const params = {}

    this.props.executeLogin(params).then(response => {
      this.props.resetFormState()
      this.props.navigation.replace('TabGroup')
    }).catch(error => {
      console.log(error)
    })
  }

  render() {
    const { username, password } = this.props

    const renderHeader = (
      <View style={styles.header}>
        <Image source={staticImages.loginHeader} style={styles.headerImg}/>
        <Text style={styles.titleText}>VPTI Mobile</Text>
      </View>
    )

    const renderForm = (
      <View style={styles.form}>
        <InputText 
          name="username"
          value={username}
          placeholder="Username"
          customInputStyle={styles.inputText} />
        <InputText 
          name="password"
          value={password}
          placeholder="Password"
          customInputStyle={styles.inputText} />
        <TouchableOpacity
          onPress={this.onClickLogin}
          style={styles.loginButton}>
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
      </View>
    )

    const renderFooter = (
      <View style={{ flex: 1 }}>
        <View style={styles.footerTitle}>
          <Text style={styles.footerTitleText}>KSO Sucofindo - Surveyor Indonesia</Text>
        </View>
        <View style={styles.footer}>
          <View style={styles.footerLeftLogo}>
            <Image source={staticImages.logoSCI} style={styles.logo}/>
          </View>
          <View style={styles.footerRightLogo}>
            <Image source={staticImages.logoSurveyorIndonesia} style={styles.logo}/>
          </View>
        </View>
      </View>
    )

    return (
      <AppContainer header="default">
        <View style={styles.mainContainer}>
          {renderHeader}
          {renderForm}
          {renderFooter}
        </View>
      </AppContainer>
    )
  }
}

export default LoginScreen;