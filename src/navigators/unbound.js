import { createStackNavigator } from 'react-navigation'

import OpenGallery from 'screens/openGallery'

const UnboundStack = createStackNavigator(
  {
    OpenGallery: { screen: OpenGallery }
  }, 
  {
    mode: 'card',
    headerMode: 'float',
    transitionConfig: () => ({
      screenInterpolator: sceneProps => {
        // https://medium.com/async-la/custom-transitions-in-react-navigation-2f759408a053
        const { position, layout, scene, index, scenes } = sceneProps
        const toIndex = index
        const thisSceneIndex = scene.index
        const height = layout.initHeight
        const width = layout.initWidth
  
        const translateX = position.interpolate({
          inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
          outputRange: [width, 0, 0]
        })
  
        // Since we want the card to take the same amount of time 
        // to animate downwards no matter if it's 3rd on the stack 
        // or 53rd, we interpolate over the entire range 
        // from 0 - thisSceneIndex
        const translateY = position.interpolate({
          inputRange: [0, thisSceneIndex],
          outputRange: [height, 0]
        })
  
        const slideFromRight = { transform: [{ translateX }] }
        const slideFromBottom = { transform: [{ translateY }] }
  
        // Find the top screen on the stack
        const lastSceneIndex = scenes[scenes.length - 1].index
  
        // Test whether we're skipping back more than one screen
        if (lastSceneIndex - toIndex > 1) {
          // Do not transoform the screen being navigated to
          if (scene.index === toIndex) return
          return slideFromBottom
        }
  
        return slideFromRight
      },
    }),
  }
)

export default UnboundStack