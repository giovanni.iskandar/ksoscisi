import { scale, verticalScale } from 'react-native-size-matters'
import { ThemeColor, ThemeFontSize } from 'themes/variables'

export default {
  mainContainer: {
    flex: 1,
    backgroundColor: '#fff'
  },
  header: {
    flex: 1
  },
  titleText: {
    position: 'absolute',
    zIndex: 1,
    alignSelf: 'center',
    top: verticalScale(75),
    color: ThemeColor.textLight,
    fontWeight: 'bold',
    fontSize: ThemeFontSize.xxxlarge
  },
  form: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: '12%',
    backgroundColor: 'transparent'
  },
  footerTitle: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  footerTitleText: {
    fontSize: ThemeFontSize.medium,
    color: ThemeColor.textDark
  },
  footer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: scale(50),
    paddingBottom: verticalScale(10)
  },
  footerLeftLogo: {
    flex: 1,
    paddingRight: scale(5)
  },
  footerRightLogo: {
    flex: 1,
    paddingLeft: scale(5),
    paddingBottom: verticalScale(10)
  },
  logo: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain'
  },
  headerImg: {
    width: '100%',
    flex: 1
  },
  inputText: {
    borderWidth: 1,
    borderColor: '#d8d8d8',
    borderRadius: 100,
    textAlign: 'center',
    marginBottom: verticalScale(35)
  },
  loginButton: {
    backgroundColor: ThemeColor.main, 
    alignItems: 'center',
    alignSelf: 'center',
    width: scale(170),
    height: verticalScale(40),
    borderRadius: 100,
    justifyContent: 'center'
  },
  loginText: {
    color: ThemeColor.textLight,
    fontWeight: 'bold',
    fontSize: ThemeFontSize.large
  }
}

