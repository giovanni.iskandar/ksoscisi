import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { Badge } from 'native-base'
import IconFA from 'react-native-vector-icons/FontAwesome'
import { ThemeColor } from 'themes/variables'
import styles from './style'

const BoxButton = ({ data, index, totalItem, onPress }) => {
  const containerStyle = index + 1 == totalItem ? styles.boxContainerLastChild : styles.boxContainer
  const renderBadge =
    <Badge success style={styles.badge}>
      <Text style={styles.badgeText}>{data.totalChild}</Text>
    </Badge>

  return (
    <TouchableOpacity style={containerStyle} onPress={onPress}>
      <Text style={styles.titleText}>{data.name}</Text>
      <View style={styles.rightContainer}>
        { data.totalChild ? renderBadge : null }
        <IconFA name='angle-right' size={25} color={ThemeColor.main} />
      </View>
    </TouchableOpacity>
  )
}

export default BoxButton