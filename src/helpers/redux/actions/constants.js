export const SET_FORM = 'SET_FORM'
export const RESET_FORM = 'RESET_FORM'
export const SET_REQUEST_STATE = 'SET_REQUEST_STATE'
// Auth
export const SET_LOGGED_IN = 'SET_LOGGED_IN'
export const SET_LOGGED_OUT = 'SET_LOGGED_OUT'
