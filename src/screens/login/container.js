import { Platform } from 'react-native'
import { connect } from 'react-redux';
import Component from './component';
import { resetFormState } from 'helpers/redux/actions/formActions'
import { executeLogin } from 'helpers/redux/actions/authActions'

const mapStateToProps = (state) => ({ 
  // form: state.form
  username: state.form.username,
  password: state.form.password
  // state: state,
  // brandInObject: state.entities.brands.byId,
  // brandInArray: state.entities.brands.byId ? Object.values(state.entities.brands.byId) : []
});

const mapDispatchToProps = (dispatch) => ({
  resetFormState: () => {
    dispatch(resetFormState())
  },
  executeLogin: (params) => {
    return dispatch(executeLogin(params))
  }
  // executeLogin: executeLogin,
  // dispatch: dispatch
});

const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(Component);

export default LoginContainer;
