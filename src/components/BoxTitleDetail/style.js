import { scale, verticalScale } from 'react-native-size-matters'
import { ThemeColor, ThemeFontSize, ThemeBorder, ThemeGap } from 'themes/variables'

export default {
  mainContainer: {
    flex: 1,
    backgroundColor: ThemeColor.original
  },
  boxContainer: {
    backgroundColor: ThemeColor.base,
    borderRadius: ThemeBorder.radius,
    elevation: ThemeBorder.elevation,
    marginTop: verticalScale(15),
    marginHorizontal: ThemeGap.outerHorizontal,
    paddingBottom: verticalScale(10)
  },
  get boxContainerLastChild () {
    return {
      ...this.boxContainer,
      marginBottom: verticalScale(25)
    }
  },
  titleContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: ThemeGap.innerHorizontal,
    paddingTop: scale(10),
    paddingBottom: scale(6),
    borderBottomWidth: 1,
    borderColor: ThemeColor.lightDark
  },
  titleTextContainer: {
    flex: 1,
    marginLeft: scale(10)
  },
  titleText: {
    fontWeight: 'bold',
    fontSize: ThemeFontSize.large,
    color: ThemeColor.textDark
  },
  
  detailContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingHorizontal: ThemeGap.innerHorizontal,
    paddingTop: scale(10),
    paddingBottom: scale(10)
  },
  detailLabelContainer: {
    flex: 1,
    marginLeft: scale(18)
  },
  detailLabelText: {
    fontSize: ThemeFontSize.medium,
    color: ThemeColor.textDark
  },
  imageFlag: {
    height: scale(26),
    width: scale(26),
    borderRadius: scale(15),
    borderWidth: 1,
    borderColor: ThemeColor.lightDark
  },
  iconSize: {
    height: scale(24),
    width: scale(24),
    resizeMode: 'contain'
  }
}