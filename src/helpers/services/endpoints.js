// Auth
export const SV_EXECUTE_LOGIN = 'executeLogin'
export const SV_EXECUTE_LOGOUT = 'executeLogout'

// Contact
export const SV_GET_CONTACT_LIST = 'getContactList'

// Profile
export const SV_GET_PROFILE_DETAILS = 'getProfileDetails'

// Branch
export const SV_GET_BRANCH_LIST = 'getBranchList'

// Affiliate
export const SV_GET_AFFILIATE_LIST = 'getAffiliateList'

// Tracking VO
export const SV_GET_VO_BY_NAME = 'getVoByName'
export const SV_GET_VO_PARTIAL = 'getVoPartial'
export const SV_GET_VO_SPLIT = 'getVoSplit'
export const SV_GET_VO_DETAIL = 'getVoDetail'