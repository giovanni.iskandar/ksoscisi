/* Available Input props - http://facebook.github.io/react-native/docs/textinput.html */

import React, { Component } from "react";
import { View, Image, TouchableOpacity } from "react-native";
import { Item, Label, Input } from "native-base";
import enhancedForm from "helpers/HOC/enhancedForm";
import style from "./style";

class InputTextClass extends Component {
  
  render(){
    const { customItemStyle, customInputStyle, label, name, maxLength, placeholder, multiline, keyboardType, editable, placeholderTextColor } = this.props
    const { handleInputChange, wrapperState } = this.props
    const { value } = wrapperState
    
    return (
      <View>
        <Item
          underline={false}
          style={customItemStyle ? { ...customItemStyle } : { ...style.itemStyle, borderColor: 'transparent' }}>
          <Input
            value={value}
            name={name}
            underline={false}
            maxLength={maxLength ? maxLength : 50}
            placeholder={placeholder ? placeholder : ''}
            multiline={multiline ? multiline : false}
            keyboardType={keyboardType ? keyboardType : 'default'}
            editable={editable ? editable : true}
            onEndEditing={() => {}}
            onFocus={() => {}}
            onChangeText={handleInputChange}
            style={customInputStyle ? customInputStyle : style.inputStyle}
            placeholderTextColor={placeholderTextColor ? placeholderTextColor : 'gray'}
          />
        </Item>
      </View>
    )
  }
}

// InputTextClass.propTypes = {
//   transparent: PropTypes.bool,
//   normal: PropTypes.bool,
//   placeholder: PropTypes.string,
//   name: PropTypes.string,
//   label: PropTypes.string,
//   multiline: PropTypes.bool,
//   secure: PropTypes.bool,
//   centered: PropTypes.bool,
//   icon: PropTypes.string,
//   helper: PropTypes.string,
//   maxLength: PropTypes.number,
//   floatingLabel: PropTypes.bool,
//   inlineLabel: PropTypes.bool,
//   right: PropTypes.bool,
//   textPlaceholderStyle: PropTypes.object
// };

const InputText = enhancedForm(InputTextClass)
export default InputText