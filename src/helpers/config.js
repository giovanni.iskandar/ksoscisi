export const API_URL = "http://10.247.37.199:3000/" // Local Gio
// export const API_URL = "http://192.168.43.87:3000/" // Gio XL

// hit running mock server API
// export const ENV = 'DEV'

// hit real API
// export const ENV = 'PROD'

// hit static API on http://myjson.com/api
export const ENV = 'STATIC'