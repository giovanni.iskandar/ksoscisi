import { API_URL, ENV } from 'helpers/config'
import { store } from 'helpers/redux/store'
import { setRequestState } from 'helpers/redux/actions/genericActions'
import * as types from './endpoints'

class ApiService {
  constructor() {

  }

  mapStaticApi = (path) => {
    let staticApiPath;
    switch(path) {
      case types.SV_EXECUTE_LOGIN:
      case types.SV_EXECUTE_LOGOUT:
        staticApiPath = 'https://api.myjson.com/bins/q38xm'
        break
      case types.SV_GET_CONTACT_LIST:
        staticApiPath = 'https://api.myjson.com/bins/9h95m'
        break
      case types.SV_GET_PROFILE_DETAILS:
        staticApiPath = 'https://api.myjson.com/bins/1gcgpm'
        break
      case types.SV_GET_BRANCH_LIST:
        staticApiPath = 'https://api.myjson.com/bins/8vtju'
        break
      case types.SV_GET_AFFILIATE_LIST:
        staticApiPath = 'https://api.myjson.com/bins/dnae2'
        break
      case types.SV_GET_VO_BY_NAME:
        staticApiPath = 'https://api.myjson.com/bins/zobru'
        break
      case types.SV_GET_VO_PARTIAL:
        staticApiPath = 'https://api.myjson.com/bins/ih43u'
        break
      case types.SV_GET_VO_SPLIT:
        staticApiPath = 'https://api.myjson.com/bins/7rb7u'
        break
      case types.SV_GET_VO_DETAIL :
        staticApiPath = 'https://api.myjson.com/bins/1emiru'
        break
    }
    return staticApiPath
  }

  execute = ({ path, params }) => {
    params = params ? params : {}

    const setRequest = (inProgress, error) => {
      return setRequestState({
        [path]: {
          inProgress: inProgress,
          error: error
        }
      })
    }

    store.dispatch(setRequest(true, false))
    
    const successCallback = (result, resolve) => {
      console.log('%c RESOLVED ', 'background: #2aa000; color: #fff')
      console.log(result)
      store.dispatch(setRequest(false, false))
      resolve(result)
    }

    const errorCallback = (error, reject) => {
      console.log('%c REJECTED ', 'background: #bf0000; color: #fff')
      console.log(error)
      store.dispatch(setRequest(false, true))
      reject(error)
    }

    return new Promise((resolve, reject) => {
      let payload = params
      let apiPath, options

      switch(ENV) {
        case 'DEV':
          apiPath = API_URL + path
          options = {
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify(payload)
          }
          break
        case 'PROD':
        case 'STATIC':
          apiPath = this.mapStaticApi(path)
          options = {
            type:"POST",
            data:'{"key":"value"}',
            contentType:"application/json; charset=utf-8",
            dataType:"json",
          }
          break
        default:
          apiPath = API_URL + path
      }

      const apiLogText = apiPath
      console.log('%cExecuting API ===> ' + '%c' + apiLogText, 'font-style: italic; font-size: 14px; font-weight: bold; color: #c600c0', 'font-style: normal')

      fetch(apiPath, options)
        .then(response => response.json())
        .then(result => successCallback(result, resolve))
        .catch(error => errorCallback(error, reject))

      // fetch(apiPath, {
      //   method: "POST",
      //   headers: {
      //     "Content-Type": "application/json"
      //   },
      //   body: JSON.stringify(payload)
      // }).then((response) => response.json()
      // ).then(result => successCallback(result, resolve)
      // ).catch(error => errorCallback(error, reject))

      // fetch(apiPath, {
      //   type:"POST",
      //   data:'{"key":"value"}',
      //   contentType:"application/json; charset=utf-8",
      //   dataType:"json",
      // }).then((response) => response.json()
      // ).then(result => successCallback(result, resolve)
      // ).catch(error => errorCallback(error, reject))

    })
  }


}

export default new ApiService()