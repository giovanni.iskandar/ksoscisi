import { connect } from 'react-redux';
import Component from './component';
import { executeLogout } from 'helpers/redux/actions/authActions'

const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => ({
  executeLogout: (params) => {
    return dispatch(executeLogout(params))
  }
});

const Container = connect(mapStateToProps, mapDispatchToProps)(Component);

export default Container;
