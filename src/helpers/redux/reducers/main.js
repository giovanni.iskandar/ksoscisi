import { combineReducers } from "redux";
import { setLoggedIn } from './authReducer'

const mainReducer = combineReducers({
  isLoggedIn: setLoggedIn,
});

export default mainReducer;