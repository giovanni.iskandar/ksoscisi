import React from 'react'
import { View, Text, TouchableHighlight, Image, TouchableOpacity } from 'react-native'
import { Container, Header, Content, List, ListItem } from 'native-base'

const renderTab1 = ({ item }) => {
  return (
    <Container>
      <Content>
        <List>
          <ListItem itemDivider>
            <Text>Verification Request</Text>
          </ListItem>                    
          <ListItem style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>01. Verification Request Date</Text>
            <Text>12 December 2018 19:00:00</Text>
            <Text style={{ marginTop: 10 }}>VR Submit No 123123123123</Text>
          </ListItem>
          <ListItem itemDivider>
            <Text>Verification Order</Text>
          </ListItem>
          <ListItem style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>02. Verification Order Date</Text>
            <Text>12 December 2018 19:00:00</Text>
          </ListItem>
          <ListItem style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>03. VO Transfer to ASDQWKR</Text>
            <Text>12 December 2018 19:00:00</Text>
          </ListItem>
          <ListItem itemDivider>
            <Text >Inspection</Text>
          </ListItem>
          <ListItem style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>04. Inspection</Text>
            <Text>12 December 2018 19:00:00</Text>
          </ListItem>
          <ListItem style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>05. Inspection</Text>
            <Text>12 December 2018 19:00:00</Text>
          </ListItem>
          <ListItem style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>06. Inspection</Text>
            <Text>12 December 2018 19:00:00</Text>
          </ListItem>
          <ListItem style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>07. Inspection</Text>
            <Text>12 December 2018 19:00:00</Text>
          </ListItem>
          <ListItem style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>08. Inspection</Text>
            <Text>12 December 2018 19:00:00</Text>
          </ListItem>
        </List>
      </Content>
    </Container>
  )
}

export default renderTab1;