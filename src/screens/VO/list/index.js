import React, { Component } from 'react';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Item, Input } from "native-base";
import { View, Text, FlatList, ActivityIndicator, TouchableOpacity } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';

export default class VOList extends React.Component {
  state = {
    data: [],
    isDateTimePickerVisible: false,
  };

  componentDidMount() {
    this.getData()
  };

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {
    console.log('A date has been picked: ', date);
    this._hideDateTimePicker();
  };

  getData = () => {
    let res = {
      results: [
        {
          id: '001',
          name: 'X.16.000000',
          partial: [
            {
              id: '001-001',
              name: 'Partial 1 - 1212121211'
            },
            {
              id: '001-002',
              name: 'Partial 2 - 2323232232'
            }
          ],
          createdDate: 1546583642000,
          updatedDate: 1546583642000
        },
        {
          id: '002',
          name: 'X.16.111111',
          createdDate: 1546583642000,
          updatedDate: 1546583642000
        },
        {
          id: '003',
          name: 'X.16.222222',
          createdDate: 1546670042000,
          updatedDate: 1546670042000
        },
        {
          id: '004',
          name: 'X.16.333333',
          createdDate: 1546670042000,
          updatedDate: 1546670042000
        },
        {
          id: '005',
          name: 'X.16.444444',
          createdDate: 1546756442000,
          updatedDate: 1546756442000
        },
        {
          id: '006',
          name: 'X.16.555555',
          createdDate: 1546756442000,
          updatedDate: 1546756442000
        },
        {
          id: '007',
          name: 'X.16.666666',
          createdDate: 1546756442000,
          updatedDate: 1546756442000
        }
      ],
      error: false
    }
    this.setState({
      data: res.results
    }, () => {
      this.data = this.state.data
    })
  };

  filterData = (text) => {
    let filteredData = this.data.filter(item => item.name.includes(text))
    this.setState({ data: filteredData })
  };

  clickItem = (item) => {
    console.log('asdasd: ', item.name)
    this.props.navigation.navigate('Detail')
  };

  renderSeparator = () => {
    return (
      <View style={{ height: 1, width: '100%', backgroundColor: '#CED0CE' }} />
    );
  };

  renderItem = ({item}) => {
    return (
      <TouchableOpacity style={{ paddingHorizontal: 10, height: 60, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} onPress={() => this.clickItem(item)}>
        <Text style={{ fontSize: 18 }}>{item.name}</Text>
      </TouchableOpacity>
    )
  };

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}>
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>Verification Order List</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Item style={{ flex: 1 }} rounded>
              <Icon active name='search' />
              <Input placeholder='Search by name...' onChangeText={(text) => this.filterData(text)}/>
            </Item>
            <TouchableOpacity style={{ marginLeft: 15, marginRight: 10 }} onPress={this._showDateTimePicker}>
              <Icon name="md-calendar" />
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1 }}>
            <DateTimePicker
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDateTimePicker}
            />
          </View>
          <FlatList
            style={{ marginTop: 10 }}
            data={this.state.data}
            renderItem={this.renderItem}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={this.renderSeparator}
          />
        </Content>
      </Container>
    );
  }
}