import React from 'react'
import { View, Text, TouchableHighlight, Image, TouchableOpacity } from 'react-native'
import { Container, Header, Content, List, ListItem } from 'native-base'
import CheckBox from '../../../components/CheckBox'

const renderTab2 = ({ item }) => {
  return (
    <Container>
      <Content>
        <List>
          <ListItem itemDivider>
            <Text>Final</Text>
          </ListItem>                    
          <ListItem style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>9. Invoice No</Text>
          </ListItem>
          <ListItem style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>10. BL No</Text>
          </ListItem>
          <ListItem style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>11. Final Document</Text>
            <View style={{ flexDirection: 'row', marginTop: 10 }}>
              <CheckBox style={{ marginRight: 15 }} text="Packing List" checked={true} />
              <CheckBox style={{ marginRight: 15 }} text="Invoice" checked={true} />
              <CheckBox style={{ marginRight: 15 }} text="Bill" checked={false} />
              <CheckBox style={{ marginRight: 15 }} text="Other" checked={false} />
            </View>
          </ListItem>
          <ListItem style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>12. Draft</Text>
            <Text>12 December 2018 19:00:00</Text>
          </ListItem>
        </List>
      </Content>
    </Container>
  )
}

export default renderTab2;