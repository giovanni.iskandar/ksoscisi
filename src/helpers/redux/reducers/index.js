import { combineReducers } from 'redux'
import mainReducer from './main'
import * as types from '../actions/constants'

function formReducer(state = {}, action) {
  switch (action.type) {
    case types.SET_FORM:
      return {
        ...state,
        ...action.form
      };
      break;
    case types.RESET_FORM:
      return {};
      break;
    default:
      return state;
      break;
  }
}

export function setRequestState(state = {}, action) {
  switch (action.type) {
    case types.SET_REQUEST_STATE:
      return {
        ...state,
        ...action.payload
      }
    default:
      return state;
  }
}

export default function createReducer() {
  return combineReducers({
    route: {},
    global: {},
    requests: setRequestState,
    form: formReducer,
    main: mainReducer
  })
}