import { scale, verticalScale } from 'react-native-size-matters'
import { ThemeColor, ThemeFontSize, ThemeBorder, ThemeGap } from 'themes/variables'

export default {
  mainContainer: {
    flex: 1,
    backgroundColor: ThemeColor.original
  },
  baseBorderStyle: {
    backgroundColor: ThemeColor.base,
    borderRadius: ThemeBorder.radius,
    elevation: ThemeBorder.elevation,
    marginHorizontal: ThemeGap.outerHorizontal
  },
  get profileSectionContainer() {
    return {
      ...this.baseBorderStyle,
      paddingHorizontal: ThemeGap.innerHorizontal,
      paddingTop: verticalScale(60),
      paddingBottom: verticalScale(20),
      marginTop: verticalScale(70),
      marginBottom: verticalScale(15)
    }
  },
  titleDetailText: {
    alignSelf: 'center',
    color: ThemeColor.textDark,
    fontWeight: 'bold',
    fontSize: ThemeFontSize.xlarge,
    marginBottom: verticalScale(8)
  },
  detailText: {
    color: ThemeColor.textDark,
    fontSize: ThemeFontSize.medium
  },
  profileImgContainer: {
    backgroundColor: ThemeColor.base,
    height: scale(98),
    width: scale(98),
    borderRadius: scale(49),
    borderWidth: scale(4),
    borderColor: ThemeColor.base,
    elevation: 6,
    position: 'absolute',
    top: verticalScale(20),
    zIndex: 1,
    alignSelf: 'center'
  },
  profileImg: {
    height: scale(90),
    width: scale(90),
    borderRadius: scale(45),
  },
  get menuSectionContainer() {
    return {
      ...this.baseBorderStyle,
      marginBottom: verticalScale(15),
    }
  },
  menuRowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: ThemeGap.innerHorizontal,
    paddingVertical: verticalScale(16)
  },
  get menuRowContainerUnderlined() {
    return {
      ...this.menuRowContainer,
      borderBottomWidth: 1,
      borderColor: ThemeColor.lightDark
    }
  },
  iconSize: {
    width: scale(22),
    height: scale(22),
    resizeMode: 'contain'
  },
  menuLabelContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: scale(12)
  },
  menuText: {
    color: ThemeColor.textDark,
    fontSize: ThemeFontSize.medium
  }
}

