import * as types from "helpers/redux/actions/constants";

export function setLoggedIn(state = false, action) {
  switch (action.type) {
    case types.SET_LOGGED_IN:
      return true;
    case types.SET_LOGGED_OUT:
      return false;
    default:
      return state;
  }
}