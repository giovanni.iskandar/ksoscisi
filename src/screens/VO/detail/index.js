import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Tabs, Tab, TabHeading } from "native-base";
import Tab1 from './tab1';
import Tab2 from './tab2';

export default class VO extends React.Component {
  render() {
    return (
      <Container>
        <Header hasTabs>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" style={{ color: "#fff" }} />
            </Button>
          </Left>
          <Body>
            <Title>Verification Order Detail</Title>
          </Body>
          <Right />
        </Header>
        <Tabs>
          <Tab heading={ <TabHeading><Text style={{ color: 'white' }}>Progress Status</Text></TabHeading>}>
            <Tab1 />
          </Tab>
          <Tab heading={ <TabHeading><Icon name="archive" /></TabHeading>}>
            <Tab2 />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}