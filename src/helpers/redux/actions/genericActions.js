import * as types from './constants'

export const setRequestState = payload => ({
  type: types.SET_REQUEST_STATE,
  payload
})