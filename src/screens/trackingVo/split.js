import React, { Component } from 'react'
import { ScrollView, View, StatusBar, Text, TouchableOpacity } from 'react-native'
import { Container, Content, Header, Button, Left, Right, Body, Icon, Item, Input } from 'native-base'
import styles from './style'
import { ThemeColor } from 'themes/variables'
import { scale } from 'react-native-size-matters'
import ApiService from 'helpers/services'
import { SV_GET_VO_SPLIT } from 'helpers/services/endpoints'
import BoxButton from 'components/BoxButton'
import { Spinner } from 'native-base'

class SplitScreen extends Component {
  static navigationOptions = {
    title: 'Split',
    headerStyle: {
      backgroundColor: ThemeColor.main
    },
    headerTintColor: ThemeColor.base,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }

  state = {
    splitList: null
  }

  componentDidMount() {
    this.getVoSplit(this.props.navigation.state.params.id)
  }

  getVoSplit = async (partialId) => {
    let request = {
      path: SV_GET_VO_SPLIT,
      params: {
        id: partialId
      }
    }

    try {
      let response = await ApiService.execute(request)
      this.setState({ splitList: response.details })
    }
    catch(ex) {}
  }

  render() {
    const { splitList } = this.state

    const SplitList = ({ list }) => {
      const totalItem = list.length
      return list.map((item, index) =>
        <BoxButton
          key={index}
          data={item}
          index={index}
          totalItem={totalItem}
          onPress={() => this.props.navigation.navigate('VoDetail', { id: item.id })}
        />)
    }

    return (
      <ScrollView style={styles.mainContainer}>
        {
          splitList
          ? <SplitList list={splitList} />
          : <Spinner color={ThemeColor.main} />
        }
      </ScrollView>
    )
  }
}

export default SplitScreen;