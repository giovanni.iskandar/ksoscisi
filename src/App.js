import React from 'react'
import { Root } from 'native-base'
import AppNavigator from 'navigators'
import { Provider } from 'react-redux'
import { store } from 'helpers/redux/store'

export default () =>
	<Root>
		<Provider store={store}>
			<AppNavigator />
		</Provider>
	</Root>;

//Dont remove this line : Disable Yellow Box Warning Globally
console.disableYellowBox = true
