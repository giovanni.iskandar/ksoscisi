import React, { Component } from 'react'
import { ScrollView, View, StatusBar, Text, TouchableOpacity } from 'react-native'
import { Container, Content, Header, Button, Left, Right, Body, Icon, Item, Input } from 'native-base'
import styles from './style'
import { ThemeColor } from 'themes/variables'
import { scale } from 'react-native-size-matters'
import ApiService from 'helpers/services'
import { SV_GET_VO_PARTIAL } from 'helpers/services/endpoints'
import BoxButton from 'components/BoxButton'
import { Spinner } from 'native-base'

class PartialScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.state.params.name,
      headerStyle: {
        backgroundColor: ThemeColor.main
      },
      headerTintColor: ThemeColor.base,
      headerTitleStyle: {
        fontWeight: 'bold',
      }
    }
  }

  state = {
    partialList: null
  }

  componentDidMount() {
    this.getVoPartial(this.props.navigation.state.params.id)
  }

  getVoPartial = async (voId) => {
    let request = {
      path: SV_GET_VO_PARTIAL,
      params: {
        id: voId
      }
    }

    try {
      let response = await ApiService.execute(request)
      this.setState({ partialList: response.details })
    }
    catch(ex) {}
  }

  render() {
    const { partialList } = this.state

    const PartialList = ({ list }) => {
      const totalItem = list.length
      return list.map((item, index) =>
        <BoxButton
          key={index}
          data={item}
          index={index}
          totalItem={totalItem}
          onPress={() => this.props.navigation.navigate('VoSplit', { id: item.id })}
        />)
    }

    return (
      <ScrollView style={styles.mainContainer}>
        {
          partialList
          ? <PartialList list={partialList} />
          : <Spinner color={ThemeColor.main} />
        }
      </ScrollView>
    )
  }
}

export default PartialScreen;