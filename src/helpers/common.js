import { Dimensions, Platform } from 'react-native';

const staticImages = {
  loginHeader: require('assets/images/login-header.png'),
  logoSCI: require('assets/images/logo-sci.gif'),
  logoSurveyorIndonesia: require('assets/images/logo-surveyor-indonesia.gif'),
  iconMenuTrackingVo: require('assets/images/icons/menu-tracking-vo.png'),
  iconMenuBranch: require('assets/images/icons/menu-branch.png'),
  iconMenuContact: require('assets/images/icons/menu-contact.png'),
  iconMenuAffiliate: require('assets/images/icons/menu-affiliate.png'),
  iconContactApplication: require('assets/images/icons/contact-aplikasi.png'),
  iconContactFinance: require('assets/images/icons/contact-keuangan.png'),
  iconContactOffice: require('assets/images/icons/contact-office-number.png'),
  iconContactVRVOLS: require('assets/images/icons/contact-vr-vo-ls.png'),
  iconProfileAddress: require('assets/images/icons/profile-alamat.png'),
  iconProfileLicense: require('assets/images/icons/profile-license.png'),
  iconProfileEmail: require('assets/images/icons/profile-email.png'),
  iconProfileDocument: require('assets/images/icons/profile-data-pengurusan.png'),
  iconProfileSendReport: require('assets/images/icons/profile-data-pengiriman-ls.png'),
  iconProfileFinancialData: require('assets/images/icons/profile-keuangan.png'),
  iconProfileLogout: require('assets/images/icons/profile-logout.png'),
  iconGenericAddress: require('assets/images/icons/generic-address.png'),
  iconGenericPhone: require('assets/images/icons/generic-phone.png'),
  iconGenericFax: require('assets/images/icons/generic-fax.png'),
  iconGenericEmail: require('assets/images/icons/generic-email.png'),
}

const getRouting = (routeName, params, action) => {
  return {
    routeName,
    params,
    action,
    key: routeName
  }
}

const isIphoneX = () => {
  const dim = Dimensions.get('window');
  
  return (
    // This has to be iOS
    Platform.OS === 'ios' &&
    
    // Check either, iPhone X or XR
    (isIPhoneXSize(dim) || isIPhoneXrSize(dim))
  );
}

const isIPhoneXSize = (dim) => {
  return dim.height == 812 || dim.width == 812;
}

const isIPhoneXrSize = (dim) => {
  return dim.height == 896 || dim.width == 896;
}

export { staticImages, getRouting, isIphoneX }