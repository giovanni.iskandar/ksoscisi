import * as types from './constants'

// export function setFormState(form) {
//   return {
//     type: types.SET_FORM,
//     form: form
//   }
// }

// export function resetFormState() {
//   return {
//     type: types.RESET_FORM
//   }
// }

export const setFormState = form => ({
  type: types.SET_FORM,
  form
})

export const resetFormState = () => ({
  type: types.RESET_FORM
})