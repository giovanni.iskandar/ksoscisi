import { scale, verticalScale } from 'react-native-size-matters'
import { ThemeColor, ThemeFontSize, ThemeBorder, ThemeGap } from 'themes/variables'

export default {
  mainContainer: {
    backgroundColor: ThemeColor.original
  },
  partialItemContainer: {
    backgroundColor: ThemeColor.base
  },
  voItemContainer: {
    backgroundColor: ThemeColor.base
  }
}

