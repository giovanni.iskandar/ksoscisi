import React, { Component } from 'react'
import { View, Text, Linking, ScrollView, Image } from 'react-native'
import { Spinner } from 'native-base'
import styles from './style'
import IconFA from 'react-native-vector-icons/FontAwesome'
import { staticImages } from 'helpers/common'
import ApiService from 'helpers/services';
import { SV_GET_CONTACT_LIST } from 'helpers/services/endpoints'
import { ThemeColor } from 'themes/variables'

class ContactScreen extends Component {
  static navigationOptions = {
    title: 'Contact',
    headerStyle: {
      backgroundColor: ThemeColor.main,
    },
    headerTintColor: ThemeColor.base,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }

  constructor(props) {
    super(props)

    this.state = {
      contactList: null
    }
  }

  componentDidMount() {
    this.getContactList()
  }
  
  getContactList = async () => {
    let request = {
      path: SV_GET_CONTACT_LIST,
      params: {}
    }

    try {
      let response = await ApiService.execute(request)
      this.setState({ contactList: response.details })
    }
    catch(ex) { }
  }

  getIcon = (type) => {
    let imgResource;
    switch(type){
      case 'office':
        imgResource = staticImages.iconContactOffice
        break
      case 'vrvols':
        imgResource = staticImages.iconContactVRVOLS
        break
      case 'finance':
        imgResource = staticImages.iconContactFinance
        break
      case 'application':
        imgResource = staticImages.iconContactApplication
        break
      default:
        imgResource = staticImages.iconContactOffice
    }
    return imgResource
  }

  contactWA = (phoneNo) => {
    let defaultMessage = 'Hi%20there%20!!'
    const dummyUrl = 'https://wa.me/' + phoneNo + '/?text=' + defaultMessage
    Linking.canOpenURL(dummyUrl).then(supported => {
      if (supported) {
        Linking.openURL(dummyUrl);
      } else {
        console.log("Don't know how to open URI: " + dummyUrl);
      }
    })
  }

  render() {
    const Contact = ({ data, index, totalItem }) => {
      const RenderContactNo = ({ contacts }) => {
        return (
          contacts.map((contact, index) => 
            <View key={index} style={styles.contactNoRow}>
              <Text style={styles.contactNoText}>{contact}</Text>
              <IconFA.Button name="whatsapp" backgroundColor="#25d366" onPress={() => this.contactWA(contact)}>
                Whatsapp
              </IconFA.Button>
            </View>
          )
        )
      }
      const containerStyle = index + 1 == totalItem ? styles.contactContainerLastChild : styles.contactContainer
      
      return (
        <View key={index} style={containerStyle}>
          <View style={styles.titleContainer}>
          <Image source={this.getIcon(data.type)} style={styles.titleIcon}/>
            <Text style={styles.titleText}>{data.title}</Text>
          </View>
          <View style={styles.contactNoContainer}>
            <RenderContactNo contacts={data.contacts} />
          </View>
        </View>
      )
    }

    const OfficeContact = ({ data, index }) => {
      const Detail = ({ details }) => details.map((rowData, index) => {
        return (
          <View key={index} style={styles.officeDetailRow}>
            <View style={styles.officeDetailLeft}>
              <Text style={styles.contactNoText}>{rowData[0]}</Text>
            </View>
            <View style={styles.officeDetailRight}>
              <Text style={styles.contactNoText}>: {rowData[1]}</Text>
            </View>
          </View>
        )
      })

      return (
        <View key={index} style={styles.contactContainer}>
          <View style={styles.titleContainer}>
            <Image source={this.getIcon(data.type)} style={styles.titleIcon}/>
            <Text style={styles.titleText}>{data.title}</Text>
          </View>
          <View style={styles.officeDetailContainer}>
            <Detail details={data.details} />
          </View>
        </View>
      )
    }

    const ContactList = ({ list }) => {
      const totalItem = list.length
      return (
        list.map((item, index) => item.type == 'office' ?
          <OfficeContact key={index} data={item} index={index} /> :
          <Contact key={index} data={item} index={index} totalItem={totalItem} />
        )
      )
    }

    return (
      <ScrollView style={styles.mainContainer}>
        {
          this.state.contactList
          ? <ContactList list={this.state.contactList} />
          : <Spinner color={ThemeColor.main} />
        }
      </ScrollView>
    )
  }
}

export default ContactScreen