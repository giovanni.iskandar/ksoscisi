import { scale, verticalScale } from 'react-native-size-matters'
import { ThemeColor, ThemeBorder, ThemeFontSize } from 'themes/variables'

export default {
  mainContainer: {
    flex: 1,
    backgroundColor: '#fff'
  },
  profileContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: verticalScale(180),
    backgroundColor: ThemeColor.main,
    marginHorizontal: scale(25),
    marginTop: verticalScale(50),
    marginBottom: verticalScale(27),
    borderRadius: 12,
    elevation: 5
  },
  profileContainerIos: {
    justifyContent: 'center',
    alignItems: 'center',
    height: verticalScale(180),
    backgroundColor: ThemeColor.main,
    marginHorizontal: scale(25),
    marginTop: verticalScale(50),
    marginBottom: verticalScale(27),
    borderRadius: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowOpacity: 0.2,
    shadowRadius: 2
  },
  profileImgContainer: {
    backgroundColor: ThemeColor.base,
    height: scale(74),
    width: scale(74),
    borderRadius: scale(37),
    borderWidth: scale(2),
    borderColor: ThemeColor.base,
    elevation: ThemeBorder.elevation,
    alignSelf: 'center'
  },
  profileImg: {
    height: scale(70),
    width: scale(70),
    borderRadius: scale(35),
  },
  profileDetailContainer: {
    alignItems: 'center',
    marginTop: verticalScale(15)
  },
  profileNameText: {
    color: ThemeColor.base,
    fontWeight: 'bold',
    fontSize: ThemeFontSize.xxlarge
  },
  profileCommodityText: {
    color: ThemeColor.base,
    fontSize: ThemeFontSize.large
  },
  menuContainer: {
    flex: 1,
    paddingHorizontal: scale(50),
  },
  menuContainerRow: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  buttonMenuContainer: {
    backgroundColor: '#fff',
    borderRadius: 12,
    elevation: 3,
    alignItems: 'center',
    justifyContent: 'center',
    width: scale(120),
    height: scale(120),
    marginHorizontal: scale(15),
    marginVertical: scale(15)
  },
  buttonMenuContainerIos: {
    backgroundColor: '#fff',
    borderRadius: 12,
    alignItems: 'center',
    justifyContent: 'center',
    width: scale(120),
    height: scale(120),
    marginHorizontal: scale(15),
    marginVertical: scale(15),
    shadowColor: '#000',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowOpacity: 0.2,
    shadowRadius: 2
  },
  buttonMenuIcon: {
    width: scale(60),
    height: scale(60)
  },
  buttonMenuText: {
    fontWeight: 'bold',
    marginTop: verticalScale(5)
  }
}

