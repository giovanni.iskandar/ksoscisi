import React, { Component } from 'react'
import { View, Text, Linking, ScrollView, Image, TouchableOpacity, Alert, CameraRoll } from 'react-native'
import { Spinner } from 'native-base'
import styles from './style'
import IconFA from 'react-native-vector-icons/FontAwesome'
import { staticImages } from 'helpers/common'
import ApiService from 'helpers/services'
import { SV_GET_PROFILE_DETAILS } from 'helpers/services/endpoints'
import { ThemeColor } from 'themes/variables'

const MENU_ADDRESS = 'ADDRESS'
const MENU_LICENSE = 'LICENSE'
const MENU_EMAIL = 'EMAIL'
const MENU_DOCUMENT = 'DOCUMENT'
const MENU_SURVEY_REPORT = 'SURVEY_REPORT'
const MENU_FINANCIAL_DATA = 'FINANCIAL_DATA'
const MENU_LOGOUT = 'LOGOUT'

class ProfileScreen extends Component {
  static navigationOptions = {
    title: 'Profil',
    headerStyle: {
      backgroundColor: ThemeColor.main,
    },
    headerTintColor: ThemeColor.base,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }

  constructor(props) {
    super(props)

    this.state = {
      profileDetails: null,
      // photos: []
    }
  }

  componentDidMount() {
    this.getProfileDetails()
  }

  getProfileDetails = async () => {
    const request = {
      path: SV_GET_PROFILE_DETAILS,
      params: null
    }

    try {
      const response = await ApiService.execute(request)
      this.setState({ profileDetails: response.details })
    }
    catch(ex) {}
  }

  onClickLogout = () => {
    const params = {}

    this.props.executeLogout(params).then(response => {
      Alert.alert(
        "Are you sure",
        "You want to logout",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          {
            text: "OK",
            onPress: () => this.props.navigation.replace('Login')
          }
        ],
        { cancelable: false }
      )
      return null;
    }).catch(error => {

    })
  }

  menuAction = (type) => {
    switch(type) {
      case MENU_ADDRESS:
      case MENU_LICENSE:
      case MENU_EMAIL:
      case MENU_DOCUMENT:
      case MENU_SURVEY_REPORT:
      case MENU_FINANCIAL_DATA:
        break
      case MENU_LOGOUT:
        this.logout()
        break
      default:
    }
  }

  render() {
    const { profileDetails } = this.state

    const ProfileImage = () => {
      const base64Img = profileDetails.profilePicture
      return (
        <View style={styles.profileImgContainer}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('OpenGallery')}>
            <Image source={{uri: base64Img}} style={styles.profileImg} />
          </TouchableOpacity>
        </View>
      )
    }

    const RowDetail = ({ title, label, value }) => {
      const RenderTitle = <Text style={styles.titleDetailText}>{title}</Text>
      const RenderDetail = (
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 1 }}>
            <Text style={styles.detailText}>{label}</Text>
          </View>
          <View style={{ flex: 2 }}>
            <Text style={styles.detailText}>: {value}</Text>
          </View>
        </View>
      )

      return title ? RenderTitle : RenderDetail
    }

    const DetailSection = () => {
      return (
        <View style={styles.profileSectionContainer}>
          <RowDetail title={profileDetails.name} />
          <RowDetail label="KOMODITAS" value={profileDetails.commodity} />
          <RowDetail label="NPWP" value={profileDetails.npwp} />
          <RowDetail label="API" value={profileDetails.api} />
          <RowDetail label="SIUP" value={profileDetails.siup} />
        </View>
      )
    }

    const RowMenu = ({ label, icon, containerStyle, action, type }) => {
      return (
        <TouchableOpacity style={containerStyle} onPress={action}>
          <Image source={icon} style={styles.iconSize}/>
          <View style={styles.menuLabelContainer}>
            <Text style={styles.menuText}>{label}</Text>
            { type != MENU_LOGOUT ? <IconFA name='angle-right' size={25} color={ThemeColor.main} /> : null }
          </View>
        </TouchableOpacity>
      )
    }

    const MenuSection = () => {
      return (
        <View style={styles.menuSectionContainer}>
          <RowMenu label="ALAMAT" icon={staticImages.iconProfileAddress} containerStyle={styles.menuRowContainerUnderlined} action={() => this.menuAction(MENU_ADDRESS)} />
          <RowMenu label="IP/IT/SPI LICENSE" icon={staticImages.iconProfileLicense} containerStyle={styles.menuRowContainerUnderlined} action={() => this.menuAction(MENU_LICENSE)} />
          <RowMenu label="EMAIL" icon={staticImages.iconProfileEmail} containerStyle={styles.menuRowContainerUnderlined} action={() => this.menuAction(MENU_EMAIL)} />
          <RowMenu label="DATA PENGURUSAN" icon={staticImages.iconProfileDocument} containerStyle={styles.menuRowContainerUnderlined} action={() => this.menuAction(MENU_DOCUMENT)} />
          <RowMenu label="DATA PENGIRIMAN LS" icon={staticImages.iconProfileSendReport} containerStyle={styles.menuRowContainerUnderlined} action={() => this.menuAction(MENU_SURVEY_REPORT)} />
          <RowMenu label="DATA KEUANGAN" icon={staticImages.iconProfileFinancialData} containerStyle={styles.menuRowContainer} action={() => this.menuAction(MENU_FINANCIAL_DATA)} />
        </View>
      )
    }

    const LogoutMenu = () => {
      return (
        <View style={styles.menuSectionContainer}>
          <RowMenu type={MENU_LOGOUT} label="KELUAR" icon={staticImages.iconProfileLogout} containerStyle={styles.menuRowContainer} action={this.onClickLogout} />
        </View>
      )
    }

    // const renderImgs = () => {
    //   return (
    //     this.state.photos.map((p, i) =>
    //       <Image
    //         key={i}
    //         style={{
    //           width: 300,
    //           height: 100,
    //         }}
    //         source={{ uri: p.node.image.uri }}
    //       />)
    //   )
    // }

    return (
      this.state.profileDetails ?
      <ScrollView style={styles.mainContainer}>
        <ProfileImage />
        <DetailSection />
        <MenuSection />
        <LogoutMenu />
      </ScrollView> :
      <Spinner color={ThemeColor.main} />
    )
  }
}

export default ProfileScreen