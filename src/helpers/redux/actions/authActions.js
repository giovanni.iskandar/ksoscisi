import * as types from 'helpers/redux/actions/constants'
import ApiService from 'helpers/services';
import { SV_EXECUTE_LOGIN, SV_EXECUTE_LOGOUT } from 'helpers/services/endpoints'

export const setLoggedIn = () => ({
  type: types.SET_LOGGED_IN
})

export const setLoggedOut = () => ({
  type: types.SET_LOGGED_OUT
})

export const executeLogin  = params => {
  return dispatch => {
    const request = {
      path: SV_EXECUTE_LOGIN,
      params
    }

    return ApiService.execute(request).then(response => {
      dispatch(setLoggedIn())
      return response
    }).catch(error => {
      return error
    })
  }
}

export const executeLogout  = params => {
  return dispatch => {
    const request = {
      path: SV_EXECUTE_LOGOUT,
      params
    }

    return ApiService.execute(request).then(response => {
      dispatch(setLoggedOut())
      return response
    }).catch(error => {
      return error
    })
  }
}


