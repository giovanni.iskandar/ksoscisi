import React, { Component } from 'react'
import { createAppContainer, createStackNavigator } from 'react-navigation'
import Login from 'screens/login/container'
import MainStack from './main'
import UnboundStack from './unbound'
import TabGroup from './tabGroup'
// import GenericNavigator from "./generic";

// class AppNavigator extends Component{
//   render(){
//     const MainStack = createStackNavigator(
//       {
//         // ...AppNavigatorMain,
//         // ...GenericNavigator,
//         Login: {screen: Login},
//       },
//       {
//         initialRouteName:  "Login",
//         headerMode: "none",
//       }
//     );

//     return <MainStack />;
//   }
// }

// export default AppNavigator

const AppNavigator = createStackNavigator(
  {
    // ...AppNavigatorMain,
    // ...GenericNavigator,
    Login: { screen: Login },
    // ...MainStack,
    TabGroup: { screen: TabGroup },
    UnboundStack: { screen: UnboundStack }
  },
  {
    mode: 'card',
    initialRouteName: "Login",
    headerMode: "none",
    navigationOptions: params => ({
      gesturesEnabled: true,
      gesturesDirection: 'inverted',
    }),
    transitionConfig: () => ({
      screenInterpolator: sceneProps => {
        // https://medium.com/async-la/custom-transitions-in-react-navigation-2f759408a053
        const { position, layout, scene, index, scenes } = sceneProps
        const toIndex = index
        const thisSceneIndex = scene.index
        const height = layout.initHeight
        const width = layout.initWidth
  
        const translateX = position.interpolate({
          inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
          outputRange: [width, 0, 0]
        })
  
        // Since we want the card to take the same amount of time 
        // to animate downwards no matter if it's 3rd on the stack 
        // or 53rd, we interpolate over the entire range 
        // from 0 - thisSceneIndex
        const translateY = position.interpolate({
          inputRange: [0, thisSceneIndex],
          outputRange: [height, 0]
        })
  
        const slideFromRight = { transform: [{ translateX }] }
        const slideFromBottom = { transform: [{ translateY }] }
  
        // Find the top screen on the stack
        const lastSceneIndex = scenes[scenes.length - 1].index
  
        // Test whether we're skipping back more than one screen
        if (lastSceneIndex - toIndex > 1) {
          // Do not transoform the screen being navigated to
          if (scene.index === toIndex) return
          return slideFromBottom
        }
  
        return slideFromRight
      },
      // screenInterpolator: sceneProps => {
      //   const { layout, position, scene } = sceneProps;
      //   const { index } = scene;
      //   const width = layout.initWidth;
  
      //   return {
      //     opacity: position.interpolate({
      //       inputRange: [index - 1, index, index + 1],
      //       outputRange: [ 0, 1, 0],
      //     }),
      //     transform: [{
      //       translateX: position.interpolate({
      //         inputRange: [index - 1, index, index + 1],
      //         outputRange: [-width, 0, width],
      //       }),
      //     }]
      //   };
      // },
      // headerTitleInterpolator: sceneProps => {
      //   const { layout, position, scene } = sceneProps;
      //   const { index } = scene;
  
      //   return {
      //     opacity: position.interpolate({
      //       inputRange: [index - 1, index, index + 1],
      //       outputRange: [ 0, 1, 0],
      //     }),
      //     transform: [{
      //       translateX: position.interpolate({
      //         inputRange: [index - 1, index, index + 1],
      //         outputRange: [-50, 0, 50],
      //       }),
      //     }]
      //   };
      // },
    }),
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;